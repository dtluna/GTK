#!/usr/bin/env python2
import gi
gi.require_version('Gtk', '3.0')
gi.require_version('Keybinder', '3.0')

from gi.repository import Gtk, Keybinder

def callback(keystr):
    print("Event time:%d" % Keybinder.get_current_event_time())
    Gtk.main_quit()


if __name__ == '__main__':
    keystr = "<Ctrl><Alt>L"
    Keybinder.init()
    Keybinder.bind(keystr, callback)
    print("Press '%s' to handle keybinding and quit" % keystr)
    Gtk.main()
