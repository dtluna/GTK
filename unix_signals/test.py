#!/usr/bin/env python2
from gi.repository import Gtk, GLib
from posix import pipe
import signal

read_end, write_end = pipe()
print("Read end:%d, write end: %d" % (read_end, write_end))

w = Gtk.Window(title='Signal test')


def pipe_signals(sig):
    write_end.write(sig)
    print "Writing signal %d" % sig


def handle_signals(sig):
	print "Handled signal %d" % sig

signal.signal(signal.SIGINT, pipe_signals)
io_channel = GLib.IOChannel.unix_new(read_end)
io_channel.set_flags(GLib.IOFlags.NONBLOCK)
GLib.io_add_watch(io_channel, GLib.PRIORITY_HIGH, GLib.IO_IN, handle_signals, None)

if __name__ == '__main__':
	Gtk.main()
