#!/usr/bin/env python3

from gi.repository import Gtk

builder = Gtk.Builder()
builder.add_from_file("main.glade")
window = builder.get_object("window")
hide_button = builder.get_object("hide_button")
hide_button.connect('clicked', Gtk.main_quit)

if __name__ == '__main__':
    window.show_all()
    Gtk.main()
