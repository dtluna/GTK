#include <gtk/gtk.h>
#include <stdlib.h>

unsigned long count = 0;
GtkWindow *window = NULL;
GtkLabel *count_label = NULL;
gchar *label_text = NULL;

void on_count_button_clicked (GtkWidget *widget, gpointer data)
{
	count++;
	sprintf(label_text, "%d", count);
	gtk_label_set_text (count_label, label_text);
}

void destroy (GtkWidget *widget, gpointer data)
{
	gtk_main_quit();
}

int main(int argc, char *argv[])
{
	gtk_init(&argc, &argv);

	GtkBuilder* builder = gtk_builder_new_from_file ("counter.glade");

	window = (GtkWindow *)gtk_builder_get_object (builder, "window1");
	gtk_window_set_default_size(window, 200, 25);
	count_label = (GtkLabel *)gtk_builder_get_object (builder, "count_label");

	label_text = (gchar*) calloc(256, sizeof(gchar));
	if (NULL == label_text)
	{
		perror("calloc");
		gtk_main_quit();
	}
	gtk_label_set_text (count_label, "0");

	gtk_builder_connect_signals (builder, NULL);
	gtk_widget_show_all (window);
	gtk_main ();

	return 0;
}
