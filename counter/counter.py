#!/usr/bin/env python

from gi.repository import Gtk


class App(object):
    builder = Gtk.Builder()

    window = None

    count = 0
    count_label = None

    count_button = None

    def __init__(self):

        self.builder.add_from_file("counter.glade")
        self.builder.connect_signals(self)

        self.window = self.builder.get_object("window1")
        self.window.set_default_size(200, 25)
        self.count_label = self.builder.get_object("count_label")
        self.count_button = self.builder.get_object("count_button")

        self.window.show_all()

    def on_count_button_clicked(self, widget, data=None):
        self.count = self.count + 1
        self.count_label.set_text("%d" % self.count)

    def main(self):
        Gtk.main()

    def destroy(self, widget, data=None):
        Gtk.main_quit()

if __name__ == '__main__':
    app = App()
    app.main()
