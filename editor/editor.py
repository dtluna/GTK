#!/usr/bin/env python2
# -- coding: utf-8 -- 

from gi.repository import Gtk, Pango, GtkSource, GObject
from os import path
import errno
GObject.type_register(GtkSource.View)

class Config(object):
	scheme = 'classic'
	font = 'Monospace 12'
	fullscreen = False
	opened_file = 'Unnamed'

	def __init__(self):
		super(Config, self).__init__()

	def print_properties(self):
		print 'scheme\t%s\n' % self.scheme
		print 'font\t%s\n' % self.font
		print 'fullscreen\t%r\n' % self.fullscreen
		print 'opened_file\t%s\n' % self.opened_file

	def save(self):
		#self.print_properties()
		config_file = open("editor.cfg", "w")
		config_file.write('scheme\t%s\n' % self.scheme)
		config_file.write('font\t%s\n' % self.font)
		config_file.write('fullscreen\t%r\n' % self.fullscreen)
		config_file.write('opened_file\t%s\n' % self.opened_file)
		config_file.close()

	def load(self):
		if path.exists("editor.cfg") and path.isfile("editor.cfg"):
			config_file = open("editor.cfg")
			config_values = config_file.readlines()
			config_file.close()

			values = list(map(lambda s: s[s.find('\t'):].strip(), config_values))

			self.scheme = values[0]
			self.font = values[1]
			self.fullscreen = (values[2] == 'True')
			self.opened_file = values[3]
			#self.print_properties()
		else:
			self.default()
			#self.print_properties()

	def default(self):
		self.scheme = 'classic'
		self.font = 'Monospace 12'
		self.fullscreen = False
		self.opened_file = 'Unnamed'


class Application(object):
	cfg = Config()
	builder = Gtk.Builder()
	window = None
	is_fullscreen = False
	screen = None
	
	source_view = None
	source_view_buffer = None

	lang_manager = GtkSource.LanguageManager()
	scheme_manager = GtkSource.StyleSchemeManager()
	style_scheme = None

	open_file_chooser_dialog = None
	save_as_file_chooser_dialog = None
	font_chooser_dialog = None
	about_dialog = None
	theme_chooser_dialog = None

	theme_chooser_widget = None

	filename_of_opened_file = cfg.opened_file
	filename_set = (filename_of_opened_file == "Unnamed")
	filename_label = None

	def __init__(self):
		super(Application, self).__init__()

		self.cfg.load()
		self.style_scheme = self.scheme_manager.get_scheme(self.cfg.scheme)
		if self.style_scheme == None:
			self.style_scheme = self.scheme_manager.get_scheme('classic')

		self.init_builder()
		self.init_window_and_screen()
		self.init_source_view()
		self.init_source_view_buffer()
		self.init_filename_label()
		self.init_chooser_dialogs()
		self.about_dialog = self.builder.get_object("about_dialog")
		self.set_filename(self.cfg.opened_file)
		self.open_file()
		self.window.show_all()

	def init_builder(self):
		self.builder = Gtk.Builder()
		self.builder.add_from_file("editor.glade")
		self.builder.connect_signals(self)

	def init_window_and_screen(self):
		self.window = self.builder.get_object("main_window")
		if self.cfg.fullscreen == True:
			self.window.fullscreen() #— на нажатие F11
			#self.window.unfullscreen()
			self.is_fullscreen = True
		else:
			self.screen = self.window.get_screen()
			self.window.set_default_size(self.screen.get_width(), self.screen.get_height())

	def init_source_view(self):
		self.source_view = self.builder.get_object("source_view")
		self.source_view.modify_font(Pango.FontDescription(self.cfg.font))
		self.source_view.font = self.cfg.font

	def init_source_view_buffer(self):
		self.source_view_buffer = self.source_view.get_buffer()
		self.source_view_buffer.set_style_scheme(self.style_scheme)

	def init_chooser_dialogs(self):
		self.open_file_chooser_dialog = self.builder.get_object("open_file_chooser_dialog")

		self.save_as_file_chooser_dialog = self.builder.get_object("save_as_file_chooser_dialog")

		self.font_chooser_dialog = self.builder.get_object("font_chooser_dialog")

		self.theme_chooser_dialog = self.builder.get_object("theme_chooser_dialog")
		self.theme_chooser_widget = self.builder.get_object("theme_chooser_widget")		

	def init_filename_label(self):
		self.filename_label = self.builder.get_object("filename_label")
		self.filename_label.set_text(self.filename_of_opened_file)

	def run(self):
		Gtk.main()

	def reset_filename(self):
		self.filename_of_opened_file = "Unnamed"
		self.filename_set = False
		self.filename_label.set_text(self.filename_of_opened_file)

	def set_filename(self, filename):
		self.filename_of_opened_file = filename
		self.filename_set = True
		self.filename_label.set_text(filename)

	def close_app(self, widget, data=None):
		self.cfg.scheme = self.style_scheme.get_id()
		self.cfg.font = self.source_view.font
		self.cfg.fullscreen = self.is_fullscreen
		self.cfg.opened_file = self.filename_of_opened_file	
		self.cfg.save()
		Gtk.main_quit()

	def show_open_file_chooser_dialog(self, widget, data=None):
		self.open_file_chooser_dialog.run()

	def hide_open_file_chooser_dialog(self, widget, data=None):
		self.open_file_chooser_dialog.hide()

	def open_file_chosen(self, widget, data=None):
		self.set_filename(self.open_file_chooser_dialog.get_filename())
		self.open_file()
		self.open_file_chooser_dialog.hide()

	def open_file(self):
		if path.exists(self.filename_of_opened_file) and path.isfile(self.filename_of_opened_file):
			infile = open(self.filename_of_opened_file)
			lines = infile.readlines()
			infile.close()

			buf_text = ''
			for line in lines:
				buf_text = buf_text + line

			self.source_view_buffer.set_text(buf_text, len(buf_text))

			self.save_as_file_chooser_dialog.set_filename(self.filename_of_opened_file)

			lang = self.lang_manager.guess_language(self.filename_of_opened_file, buf_text)
			if lang != None:
				self.source_view_buffer.set_language(lang)
		

	def new_file(self, widget, data=None):
		self.reset_filename()
		self.source_view_buffer.set_text("", len(""))
		self.source_view.set_buffer(self.source_view_buffer)

	def save_file(self, widget, data=None):
		try:
			outfile = open(self.filename_of_opened_file, "w")
		except IOError as e: 
			if e.errno == errno.EACCES:
				self.show_eacces_dialog(e)
			return

			start_iter = self.source_view_buffer.get_iter_at_offset(0)
			end_iter = self.source_view_buffer.get_iter_at_offset(-1)

			outfile.write(self.source_view_buffer.get_text(start_iter, end_iter ,True))
			outfile.close
		else:
			self.show_save_as_file_chooser_dialog(0)

	def show_save_as_file_chooser_dialog(self, widget, data=None):
		self.save_as_file_chooser_dialog.run()

	def hide_save_as_file_chooser_dialog(self, widget, data=None):
		self.save_as_file_chooser_dialog.hide()

	def save_file_as(self, widget, data=None):
		filename = self.save_as_file_chooser_dialog.get_filename()

		try:
			outfile = open(filename, "w")
		except IOError as e: 
			if e.errno == errno.EACCES:
				self.show_eacces_dialog(e)
			return

		start_iter = self.source_view_buffer.get_iter_at_offset(0)
		end_iter =self. source_view_buffer.get_iter_at_offset(-1)

		outfile.write(self.source_view_buffer.get_text(start_iter, end_iter ,True))
		outfile.close

		self.set_filename(filename)

		self.save_as_file_chooser_dialog.hide()

	def edit_cut(self, widget, data=None):
		pass

	def edit_copy(self, widget, data=None):
		pass

	def edit_paste(self, widget, data=None):
		pass

	def edit_delete(self, widget, data=None):
		pass 

	def choose_font(self, widget, data=None):
		self.font_chooser_dialog.set_font(self.source_view.font)
		response = self.font_chooser_dialog.run()
		if response == Gtk.ResponseType.OK:
			font = self.font_chooser_dialog.get_font()
			self.source_view.modify_font(Pango.FontDescription(font))
			self.source_view.font = font
			self.font_chooser_dialog.hide()
		elif response == Gtk.ResponseType.CANCEL:
			self.font_chooser_dialog.hide()

	def show_theme_chooser_dialog(self, widget, data=None):
		self.theme_chooser_widget.set_style_scheme(self.style_scheme)
		self.theme_chooser_dialog.run()

	def hide_theme_chooser_dialog(self, widget, data=None):
		self.source_view_buffer.set_style_scheme(self.style_scheme)
		self.theme_chooser_dialog.hide()

	def try_set_theme(self, widget, data=None):
		self.source_view_buffer.set_style_scheme(self.theme_chooser_widget.get_style_scheme())

	def set_theme(self, widget, data=None):
		self.style_scheme = self.theme_chooser_widget.get_style_scheme()
		self.source_view_buffer.set_style_scheme(self.style_scheme)
		self.theme_chooser_dialog.hide()

	def help_about(self, widget, data=None):
		self.about_dialog.run()
		self.about_dialog.hide()

	def show_eacces_dialog(self, error):
		eacces_dialog = self.builder.get_object('eacces_dialog')
		label = eacces_dialog.get_content_area().get_children()[0].get_children()[0].get_children()[0]
		errmsg = "I/O error({0}): {1}: file \'{2}\'".format(error.errno, error.strerror, error.filename)
		label.set_text(errmsg)
		eacces_dialog.run()
		eacces_dialog.hide()

app = Application()
app.run()