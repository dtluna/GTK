#!/usr/bin/env python

from gi.repository import Gtk

class App:
	builder = Gtk.Builder()

	window = None

	about_dialog = None

	def __init__ (self):

		self.builder.add_from_file("backuper.glade")
		self.builder.connect_signals(self)

		self.about_dialog = self.builder.get_object("about_dialog")

		self.window = self.builder.get_object("window")
		self.window.set_default_size(200, 50)

		self.window.show_all()

	def main(self):
		Gtk.main()

	def destroy(self, widget, data=None):
		Gtk.main_quit()

	def about(self, widget, data=None):
		self.about_dialog.run()
		self.about_dialog.hide()

if __name__ == '__main__':
	app = App()
	app.main()